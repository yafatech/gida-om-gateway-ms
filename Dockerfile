FROM openjdk:16-jdk-alpine
ARG JAR_FILE=build/libs/gateway-1.0.101.jar
COPY ${JAR_FILE} gateway.jar
ENTRYPOINT ["java","-jar","/gateway.jar"]