package dev.yafatek.gateway.security.services;

import dev.yafatek.gateway.api.GoogleUser;
import dev.yafatek.gateway.api.RabbitConstants;
import dev.yafatek.gateway.api.requests.LoginRequest;
import dev.yafatek.gateway.api.respones.usersms.LoginUserResponse;
import dev.yafatek.gateway.messaging.BackendClient;
import dev.yafatek.gateway.security.utils.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class UserService {
    private final static Logger LOGGER = LoggerFactory.getLogger(UserService.class);
    private final BackendClient backendClient;

    public UserService(BackendClient backendClient) {
        this.backendClient = backendClient;
    }

    public Mono<User> findByUsername(GoogleUser googleUser) {
        // request the user from the backend.
        LoginUserResponse response = backendClient.sendAsync(new LoginRequest(googleUser.getEmail(), googleUser.getAvatar()), RabbitConstants.LOGIN_USER_KEY, RabbitConstants.USERS_EXCHANGE, LoginUserResponse.class);
//        LOGGER.info(" [*] login user response: {}", response);
        if (response.getUserName() == null)
            return Mono.just(new User(null, null, null, null, null, null, null));
        return Mono.justOrEmpty(response)
                .switchIfEmpty(Mono.defer(this::raiseError))
                .map(this::createSpringSecurityUser);
    }


    private <T> Mono<T> raiseError() {
        return Mono.error(new BadCredentialsException("username is incorrect :)"));
    }

    private User createSpringSecurityUser(LoginUserResponse user) {
        return new User(
                user.getId(),
                user.getUserName(),
                user.getEmail(),
                user.getPassword(),
                user.getAccountStatus(),
                user.getAvatar(),
                user.getRoles().parallelStream().collect(Collectors.toList()));
    }
}
