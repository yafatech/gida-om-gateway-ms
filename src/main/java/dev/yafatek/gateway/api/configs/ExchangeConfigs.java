package dev.yafatek.gateway.api.configs;

import dev.yafatek.gateway.api.RabbitConstants;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ExchangeConfigs {

    @Bean
    public DirectExchange qrExchange() {
        return new DirectExchange(RabbitConstants.QR_EXCHANGE);
    }
}
