package dev.yafatek.gateway.api.respones;

public class DashboardStatisticsResponse {
    private String productName;
    private Integer happyCustomers;
    private Double weeklySales;
    private Double monthlySales;

    public DashboardStatisticsResponse(String productName, Integer happyCustomers, Double weeklySales, Double monthlySales) {
        this.productName = productName;
        this.happyCustomers = happyCustomers;
        this.weeklySales = weeklySales;
        this.monthlySales = monthlySales;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Integer getHappyCustomers() {
        return happyCustomers;
    }

    public void setHappyCustomers(Integer happyCustomers) {
        this.happyCustomers = happyCustomers;
    }

    public Double getWeeklySales() {
        return weeklySales;
    }

    public void setWeeklySales(Double weeklySales) {
        this.weeklySales = weeklySales;
    }

    public Double getMonthlySales() {
        return monthlySales;
    }

    public void setMonthlySales(Double monthlySales) {
        this.monthlySales = monthlySales;
    }
}
