package dev.yafatek.gateway.api.respones;

import dev.yafatek.gateway.api.respones.usersms.Role;

import java.util.List;

public class AuthResponse {
    private String token;
    private String userId;
    private String username;
    private String email;
    private String avatar;
    private List<Role> roles;

    public AuthResponse() {
    }

    public AuthResponse(String token, String userId, String username, String email, String avatar, List<Role> roles) {
        this.token = token;
        this.userId = userId;
        this.username = username;
        this.email = email;
        this.roles = roles;
        this.avatar = avatar;
    }


    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
