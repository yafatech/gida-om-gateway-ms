package dev.yafatek.gateway.api;

public class GoogleUser {
    private String email;
    private String avatar;


    public GoogleUser() {
    }

    public GoogleUser(String email, String avatar) {
        this.email = email;
        this.avatar = avatar;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    @Override
    public String toString() {
        return "GoogleUser{" +
                "email='" + email + '\'' +
                ", avatar='" + avatar + '\'' +
                '}';
    }
}
