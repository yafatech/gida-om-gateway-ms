package dev.yafatek.gateway.api;

public class RabbitConstants {

    // QR
    public static final String QR_EXCHANGE = "qr:exchange";
    // keys
    public static final String QR_CREATE_KEY = "qr.createQRKey";
    public static final String QR_GET_FOR_OWNER_KEY = "qr.getAllForOwnerKey";
    public static final String DELETE_OWNER_QR_KEY = "qr.deleteOwnerQRKey";
    public static final String PUBLISH_WEBSITE_KEY = "qr.publishWebsiteKey";
    public static final String UPDATE_QR_KEY = "qr.updateQrKey";

    // USERS
    public static final String USERS_EXCHANGE = "user:Exchange";
    public static final String CREATE_USER_KEY = "user.createUserKey";
    public static final String LOGIN_USER_KEY = "user.loginUserKey";
    public static final String CREATE_BD_KEY = "user.createBDKey";
    public static final String DELETE_BD_KEY = "user.deleteBDKey";
    public static final String LOAD_BD_KEY = "user.loadBDKey";
    public static final String DELETE_USER_KEY = "user.deleteUserKey";
    public static final String CREATE_USER_PREFS_KEY = "user.createUserPrefsKey";
    public static final String GET_USER_PREFS_KEY = "user.getUserPrefsKey";
    public static final String UPDATE_USER_PREFS_KEY = "user.updateUserPrefsKey";
    public static final String DELETE_USER_PREFS_KEY = "user.deleteUserPrefsKey";
    public static final String COUNT_USERS_KEY = "users.countUsersKey";
    public static final String LOOKUP_USER_BY_DOMAIN_KEY = "users.lookupUserByDomainKey";

    // Inventory.
    public static final String INVENTORY_EXCHANGE = "inventory:exchange";
    public static final String CREATE_CATEGORY_KEY = "inventory.createCategoryKey";
    public static final String DELETE_CATEGORY_KEY = "inventory.deleteCategoryKey";
    public static final String CREATE_PRODUCT_KEY = "inventory.createProductKey";
    public static final String LOAD_OWNER_PRODUCTS_KEY = "inventory.loadOwnerProductsKey";
    public static final String LOAD_OWNER_CATEGORY_KEY = "inventory.loadOwnerCategoriesKey";
    public static final String UPDATE_OWNER_PRODUCT_KEY = "inventory.updateOwnerProductKey";
    public static final String DELETE_OWNER_PRODUCT_KEY = "inventory.deleteOwnerProductKey";
    public static final String LOOKUP_CATEGORY_DOMAIN_KEY = "inventory.lookupDomainCategoriesKey";
    public static final String CREATE_HOT_DEAL_PRODUCT_KEY = "inventory.createHotDealProductKey";
    public static final String LOAD_HOT_DEALS_KEY = "inventory.loadHotDealsKey";

    // Subscriptions
    public static final String SUBSCRIPTIONS_EXCHANGE = "subscriptions:exchange";
    public static final String CREATE_SUBSCRIPTION_KEY = "subscription.createSubscriptionKey";
    public static final String GET_OWNER_SUBSCRIPTION_KEY = "subscription.getOwnerSubscriptionKey";
    public static final String DELETE_OWNER_SUBSCRIPTION_KEY = "subscription.deleteOwnerSubscriptionKey";

    // Templating Service
    public static final String TEMPLATES_EXCHANGE = "templating:exchange";
    public static final String CREATE_TEMPLATE_KEY = "templating.createTemplateKey";
    public static final String GET_ALL_TEMPLATES_KEY = "templating.getAllTemplatesKey";
    public static final String DELETE_TEMPLATE_KEY = "templating.deleteTemplateKey";

}
