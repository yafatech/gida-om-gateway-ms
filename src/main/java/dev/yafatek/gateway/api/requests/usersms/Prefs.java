package dev.yafatek.gateway.api.requests.usersms;

public class Prefs {
    private String prefKey;
    private String prefValue;

    public Prefs() {
    }

    public String getPrefKey() {
        return prefKey;
    }

    public void setPrefKey(String prefKey) {
        this.prefKey = prefKey;
    }

    public String getPrefValue() {
        return prefValue;
    }

    public void setPrefValue(String prefValue) {
        this.prefValue = prefValue;
    }
}
