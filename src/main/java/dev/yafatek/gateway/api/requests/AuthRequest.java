package dev.yafatek.gateway.api.requests;

public class AuthRequest {
    private String googleToken;


    public String getGoogleToken() {
        return googleToken;
    }

    public void setGoogleToken(String googleToken) {
        this.googleToken = googleToken;
    }

    @Override
    public String toString() {
        return "AuthRequest{" +
                "googleToken='" + googleToken + '\'' +
                '}';
    }
}
