package dev.yafatek.gateway.api.requests.usersms;

import java.util.List;

public class CreateUserPrefsRequest {
    private String owner;
    private List<Prefs> prefs;

    public CreateUserPrefsRequest() {
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public List<Prefs> getPrefs() {
        return prefs;
    }

    public void setPrefs(List<Prefs> prefs) {
        this.prefs = prefs;
    }
}
