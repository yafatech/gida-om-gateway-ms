package dev.yafatek.gateway.api.requests.qr;

public class PublishWebsiteRequest {
    private String qrId;
    public boolean status;

    public String getQrId() {
        return qrId;
    }

    public void setQrId(String qrId) {
        this.qrId = qrId;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
