package dev.yafatek.gateway.api.requests.subscriptions;

public enum PaymentMethod {
    FREE,
    CASH,
    BANK_TRANSFER,
    CREDIT_CARD,
    DEPOSIT
}
