package dev.yafatek.gateway.api.requests;

public class CreateBDRequest {
    private String userId;
    private String websiteUrlId;
    private String phoneNumber;
    private String facebookUrl;
    private String instagramUrl;
    private Double googleLng;
    private Double googleLat;

    public CreateBDRequest() {
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getWebsiteUrlId() {
        return websiteUrlId;
    }

    public void setWebsiteUrlId(String websiteUrlId) {
        this.websiteUrlId = websiteUrlId;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getFacebookUrl() {
        return facebookUrl;
    }

    public void setFacebookUrl(String facebookUrl) {
        this.facebookUrl = facebookUrl;
    }

    public String getInstagramUrl() {
        return instagramUrl;
    }

    public void setInstagramUrl(String instagramUrl) {
        this.instagramUrl = instagramUrl;
    }

    public Double getGoogleLng() {
        return googleLng;
    }

    public void setGoogleLng(Double googleLng) {
        this.googleLng = googleLng;
    }

    public Double getGoogleLat() {
        return googleLat;
    }

    public void setGoogleLat(Double googleLat) {
        this.googleLat = googleLat;
    }

    @Override
    public String toString() {
        return "CreateBDRequest{" +
                "userId='" + userId + '\'' +
                ", websiteUrlId='" + websiteUrlId + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", facebookUrl='" + facebookUrl + '\'' +
                ", instagramUrl='" + instagramUrl + '\'' +
                ", googleLng=" + googleLng +
                ", googleLat=" + googleLat +
                '}';
    }
}
