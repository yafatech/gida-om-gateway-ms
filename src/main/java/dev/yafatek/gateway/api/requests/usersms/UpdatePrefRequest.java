package dev.yafatek.gateway.api.requests.usersms;

public class UpdatePrefRequest {
    private String prefId;
    private String prefKey;
    private String prefValue;


    public UpdatePrefRequest() {
    }


    public String getPrefId() {
        return prefId;
    }

    public void setPrefId(String prefId) {
        this.prefId = prefId;
    }

    public String getPrefKey() {
        return prefKey;
    }

    public void setPrefKey(String prefKey) {
        this.prefKey = prefKey;
    }

    public String getPrefValue() {
        return prefValue;
    }

    public void setPrefValue(String prefValue) {
        this.prefValue = prefValue;
    }
}
