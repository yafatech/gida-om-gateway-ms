package dev.yafatek.gateway.api.requests.subscriptions;

public enum SubscriptionType {
    FREE,
    STARTER,
    PREMIUM,
    ULTIMATE
}
