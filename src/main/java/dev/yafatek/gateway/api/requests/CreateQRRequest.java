package dev.yafatek.gateway.api.requests;

import org.springframework.util.StringUtils;

public class CreateQRRequest {
    private String businessName;
    private String url;
    private String owner;
    private String templateThumbnail;
    private String templateContainerName;
    private String previewUrl;

    public CreateQRRequest() {
    }

    public String getPreviewUrl() {
        return previewUrl;
    }

    public void setPreviewUrl(String previewUrl) {
        this.previewUrl = previewUrl;
    }

    public String getTemplateThumbnail() {
        return templateThumbnail;
    }

    public void setTemplateThumbnail(String templateThumbnail) {
        this.templateThumbnail = templateThumbnail;
    }

    public String getTemplateContainerName() {
        return templateContainerName;
    }

    public void setTemplateContainerName(String templateContainerName) {
        this.templateContainerName = templateContainerName;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    // make sure we have a valid url.
    public String getUrl() {
        return StringUtils.replace(this.url, " ", "%20");
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

}

