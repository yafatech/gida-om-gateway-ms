package dev.yafatek.gateway.api.requests.usersms;

public class CreateUserRequest {
    private String email;
    private String avatar;


    public CreateUserRequest(String email, String avatar) {
        this.email = email;
        this.avatar = avatar;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
