package dev.yafatek.gateway.api.requests.templatingms;

import java.util.List;

public class CreateTemplateRequest {
    private String templateName;
    private String templateUrl;
    private String templateDockerName;
    private Double templatePrice;
    private String priceSymbol;
    private String description;
    private List<String> thumbs;

    public String getTemplateName() {
        return templateName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public String getTemplateUrl() {
        return templateUrl;
    }

    public void setTemplateUrl(String templateUrl) {
        this.templateUrl = templateUrl;
    }

    public String getTemplateDockerName() {
        return templateDockerName;
    }

    public void setTemplateDockerName(String templateDockerName) {
        this.templateDockerName = templateDockerName;
    }

    public Double getTemplatePrice() {
        return templatePrice;
    }

    public void setTemplatePrice(Double templatePrice) {
        this.templatePrice = templatePrice;
    }

    public String getPriceSymbol() {
        return priceSymbol;
    }

    public void setPriceSymbol(String priceSymbol) {
        this.priceSymbol = priceSymbol;
    }

    public List<String> getThumbs() {
        return thumbs;
    }

    public void setThumbs(List<String> thumbs) {
        this.thumbs = thumbs;
    }
}
