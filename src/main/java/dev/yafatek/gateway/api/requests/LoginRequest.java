package dev.yafatek.gateway.api.requests;

public class LoginRequest {
    private String email;
    private String avatar;


    public LoginRequest() {
    }

    public LoginRequest(String email, String avatar) {
        this.email = email;
        this.avatar = avatar;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
}
