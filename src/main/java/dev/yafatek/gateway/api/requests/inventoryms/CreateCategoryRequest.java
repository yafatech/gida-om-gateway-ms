package dev.yafatek.gateway.api.requests.inventoryms;

public class CreateCategoryRequest {
    private String owner;
    private String categoryName;
    private String categoryFriendlyName;
    private String sourceWebsite;
    private String thumbnail;
    private String description;

    public CreateCategoryRequest() {
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryFriendlyName() {
        return categoryFriendlyName;
    }

    public void setCategoryFriendlyName(String categoryFriendlyName) {
        this.categoryFriendlyName = categoryFriendlyName;
    }

    public String getSourceWebsite() {
        return sourceWebsite;
    }

    public void setSourceWebsite(String sourceWebsite) {
        this.sourceWebsite = sourceWebsite;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
