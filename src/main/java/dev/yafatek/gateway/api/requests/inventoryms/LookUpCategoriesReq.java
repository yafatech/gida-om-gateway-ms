package dev.yafatek.gateway.api.requests.inventoryms;

public class LookUpCategoriesReq {
    private String owner;
    private String url;

    public LookUpCategoriesReq(String owner, String url) {
        this.owner = owner;
        this.url = url;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
