package dev.yafatek.gateway.api.requests.inventoryms;

public class LoadHotDealsRequest {
    private String owner;
    private String sourceWebsite;

    public LoadHotDealsRequest(String owner, String sourceWebsite) {
        this.owner = owner;
        this.sourceWebsite = sourceWebsite;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getSourceWebsite() {
        return sourceWebsite;
    }

    public void setSourceWebsite(String sourceWebsite) {
        this.sourceWebsite = sourceWebsite;
    }
}
