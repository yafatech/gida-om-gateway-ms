package dev.yafatek.gateway.api.requests.qr;

public class UpdateQRRequest {
    private String qrId;
    private String url;
    private String businessName;
    private String templateThumbnail;
    private String templateContainerName;
    private String previewUrl;

    public String getQrId() {
        return qrId;
    }

    public void setQrId(String qrId) {
        this.qrId = qrId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getTemplateThumbnail() {
        return templateThumbnail;
    }

    public void setTemplateThumbnail(String templateThumbnail) {
        this.templateThumbnail = templateThumbnail;
    }

    public String getTemplateContainerName() {
        return templateContainerName;
    }

    public void setTemplateContainerName(String templateContainerName) {
        this.templateContainerName = templateContainerName;
    }

    public String getPreviewUrl() {
        return previewUrl;
    }

    public void setPreviewUrl(String previewUrl) {
        this.previewUrl = previewUrl;
    }
}
