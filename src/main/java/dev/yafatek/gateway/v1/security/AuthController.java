package dev.yafatek.gateway.v1.security;

import dev.yafatek.gateway.api.GoogleUser;
import dev.yafatek.gateway.api.RabbitConstants;
import dev.yafatek.gateway.api.requests.AuthRequest;
import dev.yafatek.gateway.api.requests.LoginRequest;
import dev.yafatek.gateway.api.requests.usersms.CreateUserRequest;
import dev.yafatek.gateway.api.respones.AuthResponse;
import dev.yafatek.gateway.api.respones.usersms.CreateUserResponse;
import dev.yafatek.gateway.api.respones.usersms.LoginUserResponse;
import dev.yafatek.gateway.messaging.BackendClient;
import dev.yafatek.gateway.security.jwt.JWTUtil;
import dev.yafatek.gateway.security.services.UserService;
import dev.yafatek.gateway.v1.google.TokenService;
import dev.yafatek.restcore.api.enums.ApiResponseCodes;
import dev.yafatek.restcore.api.responses.ApiResponse;
import dev.yafatek.restcore.api.responses.ErrorResponse;
import dev.yafatek.restcore.api.utils.ApiUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.io.IOException;
import java.security.GeneralSecurityException;

@RestController
@RequestMapping("/api/v1/auth")
public class AuthController {

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthController.class);
    private final JWTUtil jwtUtil;
    //    private final PBKDF2Encoder passwordEncoder;
    private final UserService userService;
    // passwords encoder.
    private final BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(31); // Strength set as 12
    private final TokenService tokenService;
    private final BackendClient backendClient;

    public AuthController(JWTUtil jwtUtil, UserService userService, TokenService tokenService, BackendClient backendClient) {
        this.jwtUtil = jwtUtil;
        this.userService = userService;
        this.tokenService = tokenService;
        this.backendClient = backendClient;
    }

    @PostMapping(value = "/login")
    //   @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
//    @Async
    public Mono<ApiResponse<AuthResponse, ErrorResponse>> login(@RequestBody AuthRequest authRequest) throws GeneralSecurityException, IOException {
//        String email = tokenService.checkGoogleToken(authRequest.getGoogleToken());
        GoogleUser googleUser = tokenService.checkGoogleToken(authRequest.getGoogleToken());
//        LOGGER.info(" [*] response : {}", googleUser);
        if (googleUser.getEmail() == null)
            return Mono.just(ApiUtils.errorResponse(false, "Can't Login the user", ApiResponseCodes.RESOURCE_NOT_FOUND.name(),
                    new ErrorResponse("that is not a valid google token", ApiResponseCodes.RESOURCE_NOT_FOUND.name())));


        // register the user if he has a valid token, he is not in our database.
        // call the user service to check if we have that user already. if yes then just proceed with login
        LoginUserResponse response = backendClient.sendAsync(new LoginRequest(googleUser.getEmail(), googleUser.getAvatar()), RabbitConstants.LOGIN_USER_KEY, RabbitConstants.USERS_EXCHANGE, LoginUserResponse.class);
//        LOGGER.info("log: {}", response);
        if (response.getEmail() == null) {
            // just register, then login him.
            CreateUserResponse createUserResponse = backendClient.sendAsync(new CreateUserRequest(googleUser.getEmail(), googleUser.getAvatar()), RabbitConstants.CREATE_USER_KEY, RabbitConstants.USERS_EXCHANGE, CreateUserResponse.class);
//            LOGGER.info(" [*] register response: {}", createUserResponse);

            return doLogin(googleUser);
        }

        // otherwise, just login him, the user exist.
        return doLogin(googleUser);

    }

    private Mono<ApiResponse<AuthResponse, ErrorResponse>> doLogin(GoogleUser googleUser) {
        return userService.findByUsername(googleUser)
                .filter(e -> e.getEmail() != null)
                // match the raw password with the encoded one
                .filter(userDetails -> encoder.matches(googleUser.getEmail(), userDetails.getPassword()))
                .map(userDetails -> ApiUtils.success(true, "login success", ApiResponseCodes.SUCCESS.name(),
                        // String token, String userId, String username, String email, List<Role> roles
                        new AuthResponse(jwtUtil.generateToken(userDetails), userDetails.getUserId(), userDetails.getUsername(), userDetails.getEmail(), userDetails.getAvatar(), userDetails.getRoles())))
                .switchIfEmpty(Mono.just(
                        ApiUtils.errorResponse(false, "Can't Login the user", ApiResponseCodes.RESOURCE_NOT_FOUND.name(),
                                new ErrorResponse(googleUser.getEmail() + " not found", ApiResponseCodes.RESOURCE_NOT_FOUND.name()))
                ));
    }
}
