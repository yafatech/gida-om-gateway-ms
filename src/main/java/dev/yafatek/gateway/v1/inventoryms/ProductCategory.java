package dev.yafatek.gateway.v1.inventoryms;

import dev.yafatek.gateway.api.RabbitConstants;
import dev.yafatek.gateway.api.requests.UpdateOwnerProductRequest;
import dev.yafatek.gateway.api.requests.inventoryms.CreateProductRequest;
import dev.yafatek.gateway.messaging.BackendClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.CompletableFuture;

@RestController
@RequestMapping("/api/v1/inventory/products")
public class ProductCategory {
    private final static Logger LOGGER = LoggerFactory.getLogger(ProductCategory.class);
    private final BackendClient backendClient;

    public ProductCategory(BackendClient backendClient) {
        this.backendClient = backendClient;
    }


    @PostMapping
    @Async
    public CompletableFuture<Object> createProduct(@RequestBody CreateProductRequest createProductRequest) {
        LOGGER.info(" [.] create new product request, product: {} owner:{} ", createProductRequest.getName(), createProductRequest.getOwner());
        return CompletableFuture.completedFuture(
                backendClient.sendAsync(createProductRequest, RabbitConstants.CREATE_PRODUCT_KEY, RabbitConstants.INVENTORY_EXCHANGE, Object.class)
        );
    }

    @GetMapping
    @Async
    public CompletableFuture<Object> loadOwnerProducts(@RequestParam("owner") String owner) {
        return CompletableFuture.completedFuture(
                backendClient.sendAsync(owner, RabbitConstants.LOAD_OWNER_PRODUCTS_KEY, RabbitConstants.INVENTORY_EXCHANGE, Object.class)
        );
    }

    @PatchMapping
    @Async
    public CompletableFuture<Object> updateProduct(@RequestBody UpdateOwnerProductRequest updateOwnerProductRequest) {
        return CompletableFuture.completedFuture(
                backendClient.sendAsync(updateOwnerProductRequest,
                        RabbitConstants.UPDATE_OWNER_PRODUCT_KEY,
                        RabbitConstants.INVENTORY_EXCHANGE,
                        Object.class)
        );
    }

    @DeleteMapping(value = "/secure")
    @Async
    public CompletableFuture<Object> deleteOwnerProduct(@RequestParam("target") String target) {
        return CompletableFuture.completedFuture(
                backendClient.sendAsync(target, RabbitConstants.DELETE_OWNER_PRODUCT_KEY, RabbitConstants.INVENTORY_EXCHANGE, Object.class)
        );
    }
}
