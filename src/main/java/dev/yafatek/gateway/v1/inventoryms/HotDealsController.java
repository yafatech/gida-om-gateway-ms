package dev.yafatek.gateway.v1.inventoryms;

import dev.yafatek.gateway.api.RabbitConstants;
import dev.yafatek.gateway.api.requests.inventoryms.CreateHotDealProduct;
import dev.yafatek.gateway.api.requests.inventoryms.LoadHotDealsRequest;
import dev.yafatek.gateway.messaging.BackendClient;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.CompletableFuture;

@RestController
@RequestMapping("/api/v1/hot-deals")
public class HotDealsController {
    private final BackendClient backendClient;


    public HotDealsController(BackendClient backendClient) {
        this.backendClient = backendClient;
    }

    @PostMapping
    @Async
    public CompletableFuture<Object> createHotDeals(@RequestBody CreateHotDealProduct createHotDealProduct) {
        return CompletableFuture.completedFuture(
                backendClient.sendAsync(createHotDealProduct,
                        RabbitConstants.CREATE_HOT_DEAL_PRODUCT_KEY,
                        RabbitConstants.INVENTORY_EXCHANGE,
                        Object.class
                )
        );
    }

    @GetMapping(value = "/lookup")
    @Async
    public CompletableFuture<Object> lookUpOwnerAndWebsiteHotDeals(@RequestParam("owner") String owner,
                                                                   @RequestParam("sourceWebsite") String sourceWebsite
    ) {
        return CompletableFuture.completedFuture(
                backendClient.sendAsync(
                        new LoadHotDealsRequest(owner, sourceWebsite),
                        RabbitConstants.LOAD_HOT_DEALS_KEY,
                        RabbitConstants.INVENTORY_EXCHANGE,
                        Object.class
                )
        );
    }
}
