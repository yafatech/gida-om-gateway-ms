package dev.yafatek.gateway.v1.inventoryms;

import dev.yafatek.gateway.api.RabbitConstants;
import dev.yafatek.gateway.api.requests.inventoryms.CreateCategoryRequest;
import dev.yafatek.gateway.api.requests.inventoryms.LookUpCategoriesReq;
import dev.yafatek.gateway.messaging.BackendClient;
import io.netty.util.concurrent.CompleteFuture;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.CompletableFuture;

@RestController
@RequestMapping("/api/v1/inventory/categories")
public class CategoryController {
    private final static Logger LOGGER = LoggerFactory.getLogger(CategoryController.class);
    private final BackendClient backendClient;

    public CategoryController(BackendClient backendClient) {
        this.backendClient = backendClient;
    }


    @PostMapping
    @Async
    public CompletableFuture<Object> createCategory(@RequestBody CreateCategoryRequest createCategoryRequest) {
        LOGGER.info(" [.] creating new Category: {} for owner: {}", createCategoryRequest.getCategoryName(), createCategoryRequest.getOwner());
        return CompletableFuture.completedFuture(
                backendClient.sendAsync(createCategoryRequest, RabbitConstants.CREATE_CATEGORY_KEY, RabbitConstants.INVENTORY_EXCHANGE, Object.class)
        );
    }

    @GetMapping(value = "/lookup")
    @Async
    public CompletableFuture<Object> lookupDomainCategories(@RequestParam("owner") String owner, @RequestParam("websiteSource") String websiteSource) {
        return CompletableFuture.completedFuture(
                backendClient.sendAsync(new LookUpCategoriesReq(owner, websiteSource),
                        RabbitConstants.LOOKUP_CATEGORY_DOMAIN_KEY,
                        RabbitConstants.INVENTORY_EXCHANGE,
                        Object.class
                )
        );
    }

    @GetMapping
    @Async
    public CompletableFuture<Object> loadOwnerCategories(@RequestParam("owner") String owner) {
        return CompletableFuture.completedFuture(
                backendClient.sendAsync(owner, RabbitConstants.LOAD_OWNER_CATEGORY_KEY, RabbitConstants.INVENTORY_EXCHANGE, Object.class)
        );
    }

    @DeleteMapping(value = "/secure")
    @Async
    public CompletableFuture<Object> deleteCategory(@RequestParam("target") String target) {
        return CompletableFuture.completedFuture(
                backendClient.sendAsync(target, RabbitConstants.DELETE_CATEGORY_KEY, RabbitConstants.INVENTORY_EXCHANGE, Object.class)
        );
    }
}
