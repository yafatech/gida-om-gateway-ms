package dev.yafatek.gateway.v1.subscriptions;

import dev.yafatek.gateway.api.RabbitConstants;
import dev.yafatek.gateway.api.requests.subscriptions.CreateSubscriptionRequest;
import dev.yafatek.gateway.messaging.BackendClient;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.CompletableFuture;

@RestController
@RequestMapping("/api/v1/subs")
public class SubscriptionsController {
    private final BackendClient backendClient;

    public SubscriptionsController(BackendClient backendClient) {
        this.backendClient = backendClient;
    }

    @PostMapping
    @Async
    public CompletableFuture<Object> createSubscription(@RequestBody CreateSubscriptionRequest createSubscriptionRequest) {
        return CompletableFuture.completedFuture(
                backendClient.sendAsync(createSubscriptionRequest, RabbitConstants.CREATE_SUBSCRIPTION_KEY, RabbitConstants.SUBSCRIPTIONS_EXCHANGE, Object.class)
        );
    }

    @GetMapping
    @Async
    public CompletableFuture<Object> getOwnerSubscription(@RequestParam("owner") String owner) {
        return CompletableFuture.completedFuture(
                backendClient.sendAsync(owner, RabbitConstants.GET_OWNER_SUBSCRIPTION_KEY, RabbitConstants.SUBSCRIPTIONS_EXCHANGE, Object.class)
        );
    }

    @DeleteMapping(value = "/secure")
    @Async
    public CompletableFuture<Object> deleteSubscription(@RequestParam("target") String target) {
        return CompletableFuture.completedFuture(
                backendClient.sendAsync(target, RabbitConstants.DELETE_OWNER_SUBSCRIPTION_KEY, RabbitConstants.SUBSCRIPTIONS_EXCHANGE, Object.class)
        );
    }
}
