package dev.yafatek.gateway.v1.businessDetails;

import dev.yafatek.gateway.api.RabbitConstants;
import dev.yafatek.gateway.api.requests.CreateBDRequest;
import dev.yafatek.gateway.messaging.BackendClient;
import dev.yafatek.restcore.api.responses.ErrorResponse;
import dev.yafatek.restcore.api.utils.ApiUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.CompletableFuture;

@RestController
@RequestMapping("/api/v1/bd")
public class BusinessDetailsController {
    private static final Logger LOGGER = LoggerFactory.getLogger(BusinessDetailsController.class);
    private final BackendClient backendClient;

    public BusinessDetailsController(BackendClient backendClient) {
        this.backendClient = backendClient;
    }

    @PostMapping
    @Async
    public CompletableFuture<Object> createBD(@RequestBody CreateBDRequest createBDRequest) {
        return CompletableFuture.completedFuture(
                backendClient.sendAsync(createBDRequest, RabbitConstants.CREATE_BD_KEY, RabbitConstants.USERS_EXCHANGE, Object.class)
        );
    }

    @GetMapping
    @Async
    public CompletableFuture<Object> loadOwnerBD(@RequestParam("owner") String owner) {
        if (owner.isBlank())
            return CompletableFuture.completedFuture(
                    ApiUtils.errorResponse(false, "Can't Load data", "FAIL",
                            new ErrorResponse("owner must have a value", "FAIL"))
            );

        return CompletableFuture.completedFuture(
                backendClient.sendAsync(owner, RabbitConstants.LOAD_BD_KEY, RabbitConstants.USERS_EXCHANGE, Object.class)
        );
    }

    @DeleteMapping(value = "/secure")
    @Async
    public CompletableFuture<Object> deleteBD(@RequestParam("target") String target) {
        return CompletableFuture.completedFuture(
                backendClient.sendAsync(target, RabbitConstants.DELETE_BD_KEY, RabbitConstants.USERS_EXCHANGE, Object.class)
        );
    }
}
