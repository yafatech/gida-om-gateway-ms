package dev.yafatek.gateway.v1.google;

import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.gson.GsonFactory;
import dev.yafatek.gateway.api.GoogleUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken.Payload;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Collections;

@Component
public class TokenService {
    private static final Logger LOGGER = LoggerFactory.getLogger(TokenService.class);
    @Value("${GOOGLE_CLIENT_ID}")
    private String googleClientId;


    public GoogleUser checkGoogleToken(String googleToken) throws GeneralSecurityException, IOException {
        GoogleIdTokenVerifier verifier = new GoogleIdTokenVerifier.Builder(new NetHttpTransport(), new GsonFactory())
                // Specify the CLIENT_ID of the app that accesses the backend:
                .setAudience(Collections.singletonList(googleClientId))
                // Or, if multiple clients access the backend:
                //.setAudience(Arrays.asList(CLIENT_ID_1, CLIENT_ID_2, CLIENT_ID_3))
                .build();

// (Receive idTokenString by HTTPS POST)
        GoogleIdToken idToken = verifier.verify(googleToken);
        if (idToken != null) {
            Payload payload = idToken.getPayload();

            // Print user identifier
            String userId = payload.getSubject();
//            LOGGER.info("User ID: {}", userId);

            // Get profile information from payload
            String email = payload.getEmail();
//            boolean emailVerified = payload.getEmailVerified();
//            String name = (String) payload.get("name");
            String pictureUrl = (String) payload.get("picture");
//            String locale = (String) payload.get("locale");
//            String familyName = (String) payload.get("family_name");
//            String givenName = (String) payload.get("given_name");

            // Use or store profile information
            // ...
            LOGGER.info(" [*] google Email response: {}", email);

            return new GoogleUser(email, pictureUrl);

        } else {
            LOGGER.info("Invalid ID token.");
            return null;
        }
    }
}
