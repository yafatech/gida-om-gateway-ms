package dev.yafatek.gateway.v1.qr;

import dev.yafatek.gateway.api.RabbitConstants;
import dev.yafatek.gateway.api.requests.CreateQRRequest;
import dev.yafatek.gateway.api.requests.qr.PublishWebsiteRequest;
import dev.yafatek.gateway.api.requests.qr.UpdateQRRequest;
import dev.yafatek.gateway.messaging.BackendClient;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.CompletableFuture;

@RestController
@RequestMapping("/api/v1/qr")
public class QRController {
    private final BackendClient backendClient;

    public QRController(BackendClient backendClient) {
        this.backendClient = backendClient;
    }

    @PostMapping
    @Async
    public CompletableFuture<Object> createQr(@RequestBody CreateQRRequest createQRRequest) {
        return CompletableFuture.completedFuture(
                // T payload, String routingKey, String exchangeName
                backendClient.sendAsync(createQRRequest, RabbitConstants.QR_CREATE_KEY, RabbitConstants.QR_EXCHANGE, Object.class)
        );
    }

    @GetMapping
//    @PreAuthorize("hasRole('CLIENT') or hasRole('ADMIN')")
    @Async
    public CompletableFuture<Object> loadAllOwnerQRs(@RequestParam("owner") String owner) {
        return CompletableFuture.completedFuture(
                backendClient.sendAsync(owner, RabbitConstants.QR_GET_FOR_OWNER_KEY, RabbitConstants.QR_EXCHANGE, Object.class)
        );
    }

    @DeleteMapping
    @Async
    public CompletableFuture<Object> deleteQr(@RequestParam("target") String owner) {
        return CompletableFuture.completedFuture(
                backendClient.sendAsync(owner, RabbitConstants.DELETE_OWNER_QR_KEY, RabbitConstants.QR_EXCHANGE, Object.class)
        );
    }

    @PatchMapping
    @Async
    public CompletableFuture<Object> publishWebsite(@RequestBody PublishWebsiteRequest publishWebsiteRequest) {
        return CompletableFuture.completedFuture(
                backendClient.sendAsync(publishWebsiteRequest, RabbitConstants.PUBLISH_WEBSITE_KEY, RabbitConstants.QR_EXCHANGE, Object.class)
        );
    }

    @PutMapping(value = "/dest")
    @Async
    public CompletableFuture<Object> updateQr(@RequestBody UpdateQRRequest updateQRRequest) {
        return CompletableFuture.completedFuture(
                backendClient.sendAsync(updateQRRequest,
                        RabbitConstants.UPDATE_QR_KEY,
                        RabbitConstants.QR_EXCHANGE,
                        Object.class
                )
        );
    }
}
