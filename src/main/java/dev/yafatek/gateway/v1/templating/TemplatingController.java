package dev.yafatek.gateway.v1.templating;

import dev.yafatek.gateway.api.RabbitConstants;
import dev.yafatek.gateway.api.requests.templatingms.CreateTemplateRequest;
import dev.yafatek.gateway.messaging.BackendClient;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.CompletableFuture;

@RestController
@RequestMapping("/api/v1/templating")
public class TemplatingController {
    private final BackendClient backendClient;

    public TemplatingController(BackendClient backendClient) {
        this.backendClient = backendClient;
    }

    @PostMapping
    @Async
    public CompletableFuture<Object> createTemplate(@RequestBody CreateTemplateRequest createTemplateRequest) {
        return CompletableFuture.completedFuture(
                backendClient.sendAsync(createTemplateRequest, RabbitConstants.CREATE_TEMPLATE_KEY, RabbitConstants.TEMPLATES_EXCHANGE, Object.class)
        );
    }

    @GetMapping
    @Async
    public CompletableFuture<Object> getAllTemplates() {
        return CompletableFuture.completedFuture(
                backendClient.sendAsync(RabbitConstants.GET_ALL_TEMPLATES_KEY, RabbitConstants.TEMPLATES_EXCHANGE, Object.class)
        );
    }

    @DeleteMapping(value = "/secure")
    @Async
    public CompletableFuture<Object> deleteTemplate(@RequestParam("target") String target) {
        return CompletableFuture.completedFuture(
                backendClient.sendAsync(target, RabbitConstants.DELETE_TEMPLATE_KEY, RabbitConstants.TEMPLATES_EXCHANGE, Object.class)
        );
    }
}
