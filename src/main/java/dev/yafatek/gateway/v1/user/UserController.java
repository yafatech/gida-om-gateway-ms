package dev.yafatek.gateway.v1.user;

import dev.yafatek.gateway.api.RabbitConstants;
import dev.yafatek.gateway.api.requests.CreateUserRequest;
import dev.yafatek.gateway.api.requests.usersms.CreateUserPrefsRequest;
import dev.yafatek.gateway.api.requests.usersms.UpdatePrefRequest;
import dev.yafatek.gateway.messaging.BackendClient;
import io.netty.util.concurrent.CompleteFuture;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.CompletableFuture;

@RestController
@RequestMapping("/api/v1/users")
public class UserController {
    private final BackendClient backendClient;

    public UserController(BackendClient backendClient) {
        this.backendClient = backendClient;
    }

    @PostMapping
    @Async
    public CompletableFuture<Object> createUser(@RequestBody CreateUserRequest createUserRequest) {
        return CompletableFuture.completedFuture(
                backendClient.sendAsync(createUserRequest, RabbitConstants.CREATE_USER_KEY, RabbitConstants.USERS_EXCHANGE, Object.class)
        );
    }

    @GetMapping(value = "/lookup")
    @Async
    public CompletableFuture<Object> lookUpUserByDomain(@RequestParam("target") String target) {
        return CompletableFuture.completedFuture(
                backendClient.sendAsync(target,
                        RabbitConstants.LOOKUP_USER_BY_DOMAIN_KEY,
                        RabbitConstants.USERS_EXCHANGE,
                        Object.class

                )
        );
    }


    @GetMapping
    @Async
    public CompletableFuture<Object> getUsersCount() {
        return CompletableFuture.completedFuture(
                backendClient.sendAsync(
                        RabbitConstants.COUNT_USERS_KEY,
                        RabbitConstants.USERS_EXCHANGE,
                        Object.class
                )
        );
    }

    @DeleteMapping(value = "/secure")
    @Async
    public CompletableFuture<Object> deleteUser(@RequestParam("target") String target) {
        return CompletableFuture.completedFuture(
                backendClient.sendAsync(target, RabbitConstants.DELETE_USER_KEY, RabbitConstants.USERS_EXCHANGE, Object.class)
        );
    }


    @PostMapping(value = "/prefs")
    @Async
    public CompletableFuture<Object> createPrefs(@RequestBody CreateUserPrefsRequest createUserPrefsRequest) {
        return CompletableFuture.completedFuture(
                backendClient.sendAsync(createUserPrefsRequest,
                        RabbitConstants.CREATE_USER_PREFS_KEY,
                        RabbitConstants.USERS_EXCHANGE,
                        Object.class
                )
        );
    }

    @GetMapping(value = "/prefs")
    @Async
    public CompletableFuture<Object> getUserPrefs(@RequestParam("owner") String owner) {
        return CompletableFuture.completedFuture(
                backendClient.sendAsync(owner,
                        RabbitConstants.GET_USER_PREFS_KEY,
                        RabbitConstants.USERS_EXCHANGE,
                        Object.class)
        );
    }

    @PatchMapping(value = "/prefs")
    @Async
    public CompletableFuture<Object> updatePrefs(@RequestBody UpdatePrefRequest updatePrefRequest) {
        return CompletableFuture.completedFuture(
                backendClient.sendAsync(updatePrefRequest,
                        RabbitConstants.UPDATE_USER_PREFS_KEY,
                        RabbitConstants.USERS_EXCHANGE,
                        Object.class)
        );
    }

    @DeleteMapping(value = "/prefs/secure")
    @Async
    public CompletableFuture<Object> deleteUserPrefs(@RequestParam("target") String target) {
        return CompletableFuture.completedFuture(
                backendClient.sendAsync(target,
                        RabbitConstants.DELETE_USER_PREFS_KEY,
                        RabbitConstants.USERS_EXCHANGE,
                        Object.class)
        );
    }
}
