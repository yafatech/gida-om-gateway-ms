package dev.yafatek.gateway.v1.dashboard;

import dev.yafatek.gateway.api.respones.DashboardStatisticsResponse;
import dev.yafatek.restcore.api.enums.ApiResponseCodes;
import dev.yafatek.restcore.api.utils.ApiUtils;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.concurrent.CompletableFuture;

@RestController
@RequestMapping("/api/v1/dashboard")
public class DashboardController {

    @GetMapping(value = "/loadStatistics")
    @Async
    public CompletableFuture<Object> getStatistics() {
        ///todo: fetch the data from the services.
        return CompletableFuture.completedFuture(
                ApiUtils.success(true, "data fetched", ApiResponseCodes.SUCCESS.name(),
                        //String productName, Integer happyCustomers, Double weeklySales, Double monthlySales
                        new DashboardStatisticsResponse("Shawarma",
                                1000,
                                2500.0,
                                12000.0)
                )
        );
    }
}
