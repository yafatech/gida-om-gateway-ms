package dev.yafatek.gateway.messaging;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.AsyncRabbitTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFuture;

import java.util.concurrent.ExecutionException;

/**
 * Default Remote Services Client implementation that is used to call a methods on remote microservices.
 *
 * @author Feras E Alawadi
 * @version 1.0.1
 * @since 1.0.3
 */
@Component
public class BackendClientImpl implements BackendClient {
    private static final Logger LOGGER = LoggerFactory.getLogger(BackendClientImpl.class);
    private final AsyncRabbitTemplate asyncRabbitTemplate;
    private final ObjectMapper objectMapper;

    public BackendClientImpl(AsyncRabbitTemplate asyncRabbitTemplate, ObjectMapper objectMapper) {
        this.asyncRabbitTemplate = asyncRabbitTemplate;
        this.objectMapper = objectMapper;
    }

    @Override
    public <T, R> R sendAsync(T payload, String routingKey, String exchangeName, Class<R> clazz) {

        ListenableFuture<R> listenableFuture =
                asyncRabbitTemplate.convertSendAndReceiveAsType(
                        exchangeName,
                        routingKey,
                        payload,
                        new ParameterizedTypeReference<>() {
                        }
                );

        try {
            return objectMapper.convertValue(listenableFuture.get(), clazz);
        } catch (InterruptedException | ExecutionException e) {
            LOGGER.error(" [x] Cannot get response.", e);
            return null;
        }
    }

    @Override
    public <R> R sendAsync(String routingKey, String exchangeName, Class<R> clazz) {
        ListenableFuture<R> listenableFuture =
                asyncRabbitTemplate.convertSendAndReceiveAsType(
                        exchangeName,
                        routingKey,
                        "",
                        new ParameterizedTypeReference<>() {
                        });

        try {
            return objectMapper.convertValue(listenableFuture.get(), clazz);
        } catch (InterruptedException | ExecutionException e) {
            LOGGER.info(" [x] error calling destination service, exchange: {}", exchangeName);
            return null;
        }

    }
}